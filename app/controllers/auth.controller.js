const personService = require('../services/person.service');
const authService = require('../services/auth.service');
const jwt = require('../utils/jwt.utils');

exports.register = async (req, res) => {
  /**
   * req.body
   * -----------
   * email: string
   * password: string
   * name: string
   * lastname: string
   * * birthdate: string
   * * gender: string m/f
   */
  

  try {

    
    const personData = {
      names: req.body.names,
      lastNames: req.body.lastNames,
      email: req.body.email
    }
    // create person
    const person = await personService.create(personData);
    const userData = {
      email: req.body.email,
      password: req.body.password,
      personId: person._id
    };
    const user = await authService.create(userData);
    res.status(201).json({...user, ...person});
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

exports.login = async (req, res) => {
  const { email, password } = req.body;
  try {
    const data = await authService.login(email, password);
    const person = await personService.get(data.jwt, data.user.personId);
    res.status(200).json({ 
      user: {...data.user, ...person}, 
      token: data.jwt });
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};
