const jwt = require('jsonwebtoken');
const fs = require('fs');
const config = require('./../../config');

exports.extractToken = (req) => {
  const token = req.headers.authorization;
  if (!token) {
    return null;
  }
  return token.replace('Bearer ', '');
};

exports.validateToken = (token) => {
  const publicKey = fs.readFileSync(config.token.publicKey).toString();
  console.log('pub', publicKey);
  const properties = {algorithms: ['RS256']};
  const decoded = jwt.verify(token, publicKey, properties);
  return decoded;
};
