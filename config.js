require('dotenv').config()

module.exports = {
  port: process.env.PORT,
  token: {
    publicKey: process.env.RSA_PUBLIC_KEY_FILE
  },
  services: {
    auth: process.env.AUTH_SERVICE_URL,
    person: process.env.PERSON_SERVICE_URL,
  },
  clientApp: process.env.CLIENT_APP_URL
}