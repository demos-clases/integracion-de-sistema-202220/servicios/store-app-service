const express = require('express');
const cors = require('cors');
const config = require('./config');
const authMdw = require('./app/middlewares/jwt.middleware');
const authRoutes = require('./app/routes/auth.routes');
const personRoutes = require('./app/routes/person.routes');

const app = new express();
app.use(express.json());
var corsOptions = {
  origin: config.clientApp,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions))


app.get('/health', function(req, res) {
  const data = {
    status: 'success',
    data: {
      message: 'Api is running'
    }
  };
  res.status(200).json(data);
});

app.use('/api/v1/auth', authRoutes);
app.use('/api/v1/person', personRoutes);
app.listen(config.port, function() {
  console.info(`Servidor en express iniciado en el puerto ${config.port}`);
});