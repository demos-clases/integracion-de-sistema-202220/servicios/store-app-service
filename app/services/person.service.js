const axios = require('axios').default;;
const config = require('../../config');

exports.create = async (person) => {
  try {
    //const headers = {'Authorization': `Bearer ${token}`};
    const response = await axios.post(
      `${config.services.person}/api/v1/person`
      , person);
    if (response.status === 201) {
      return response.data;
    } else {
      throw new Error('Error al crear persona');
    }
  } catch (error) {
    console.error(error);
    throw new Error('Error al crear persona');
  }

};

exports.get = async (token, personId) => {
  try {
    const headers = {'Authorization': `Bearer ${token}`};
    const response = await axios.get(
      `${config.services.person}/api/v1/person/${personId}`, {headers});
    if (response.status === 200) {
      return response.data;
    } else {
      throw new Error('Error al crear persona');
    }
  } catch (error) {
    console.error(error);
    throw new Error('Error al crear persona');
  }

};