const personService = require('../services/person.service');
const jwt = require('../utils/jwt.utils');
exports.get = async (req, res) => {
  const { personId } = req.params;
  const token = jwt.extractToken(req);
  try {
    const person = await personService.get(token, personId);
    res.status(200).json(person);
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};
