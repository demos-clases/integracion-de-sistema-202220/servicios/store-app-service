const axios = require('axios').default;;
const config = require('../../config');

exports.create = async (user) => {
  try {
    const response = await axios.post(
      `${config.services.auth}/api/v1/auth/register`
      , user);
    if (response.status === 201) {
      return response.data;
    } else {
      throw new Error('Error al crear usuario');
    }
  } catch (error) {
    console.error(error);
    throw new Error('Error al crear usuario');
  }

};

exports.login = async (email, password) => {
  try {
    const response = await axios.post(
      `${config.services.auth}/api/v1/auth/login`
      , {email, password});
    if (response.status === 200) {
      return response.data;
    } else {
      throw new Error('Error al iniciar sesion');
    }
  } catch (error) {
    console.error(error);
    throw new Error('Error al iniciar sesion');
  }
};