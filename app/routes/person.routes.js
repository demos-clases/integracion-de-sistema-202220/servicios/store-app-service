const express = require('express');
const PersonCtrl = require('./../controllers/person.controller');

const router = express.Router();

router.get('/information/:personId', PersonCtrl.get);



module.exports = router;